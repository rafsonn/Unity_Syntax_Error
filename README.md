# Unity Syntax Error at line 232

Chat app with a built-in notepad function. The application is part of a Unity-based game project. Live chat is based on sockets.

## Built with

-   Flutter
-   SocketIO
-	SQLite

## Screenshots

<img src="./screenshots/flutter_chat.png" height="450">

## Authors

-   @rafsonn
-   @jzarzycki

## License
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
